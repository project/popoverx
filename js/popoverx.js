(function ($, Drupal, Bootstrap) {

  Bootstrap.Dialog.Handler.register('popoverx', {
    ctor: function () {
      // Extract the arguments.
      var args = Array.prototype.slice.call(arguments);
      var method = args[0];
      var options = args[1] || {};

      // Move arguments down if no method was passed.
      if ($.isPlainObject(method)) {
        options = method;
        method = null;
      }

      if (!this.dialogTrigger) {
        this.dialogTrigger = options && options.dialogOptions && options.dialogOptions.$trigger;
      }

      if (!this.dialogTrigger) {
        throw new TypeError('Unable to determine triggering element.');
      }

      options.$target = $(this.dialogTrigger);

      return $.fn.popoverX.apply(this, args).popoverX('show');
    },
    prefix: 'popover',
    themeHooks: {
      modal: 'bootstrapPopoverX',
      dialog: 'bootstrapPopoverX',
      header: 'bootstrapPopoverXHeader',
      title: 'bootstrapPopoverXTitle',
      close: 'bootstrapPopoverXClose',
      content: 'bootstrapPopoverXContent',
      body: 'bootstrapPopoverXBody',
      footer: 'bootstrapPopoverXFooter',
    }
  });

  // Extend the Bootstrap PopoverX plugin constructor class.
  if (Bootstrap.Modal.Bridge) {
    Bootstrap.extendPlugin('popoverX', Bootstrap.Modal.Bridge);
  }

  /**
   * Extend Drupal theming functions.
   */
  $.extend(Drupal.theme, /** @lend Drupal.theme */ {
    /**
     * Theme function for Bootstrap PopoverX.
     *
     * @param {Object} [variables]
     *   An object containing key/value pairs of variables.
     *
     * @return {string}
     *   The HTML for the modal.
     */
    bootstrapPopoverX: function (variables) {
      var output = '';
      var defaults = {
        attributes: {
          class: ['popover', 'popover-x', 'popover-default'],
          'data-backdrop': 'false',
          tabindex: 0,
          ariaLabelledby: 'popover-title',
          role: 'dialog'
        },
        body: '',
        closeButton: true,
        description: {
          attributes: {
            class: ['help-block']
          },
          content: null,
          position: 'before'
        },
        footer: '',
        id: 'drupal-modal',
        title: {
          attributes: {
            class: ['popover-title']
          },
          content: '',
          html: false,
          tag: 'div'
        }
      };
      variables = $.extend(true, {}, defaults, variables);

      var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
      attributes.set('id', attributes.get('id', variables.id));

      // Build the modal wrapper.
      output += '<div' + attributes + '>';

      output += '<div class="arrow"></div>';

      // Build header.
      output += Drupal.theme('bootstrapPopoverXHeader', _.omit(variables, 'attributes'));

      // Build content.
      output += Drupal.theme('bootstrapPopoverXContent', _.omit(variables, 'attributes'));

      // Build footer.
      output += Drupal.theme('bootstrapPopoverXFooter', _.omit(variables, 'attributes'));

      // Close the modal wrapper.
      output += '</div>';

      // Return the constructed modal.
      return output;
    },

    /**
     * Theme function for a Bootstrap PopoverX content.
     *
     * @param {Object} [variables]
     *   An object containing key/value pairs of variables.
     *
     * @return {string}
     *   The HTML for the modal content.
     */
    bootstrapPopoverXContent: function (variables) {
      return Drupal.theme('bootstrapPopoverXBody', variables);
    },

    /**
     * Theme function for a Bootstrap PopoverX body.
     *
     * @param {Object} [variables]
     *   An object containing key/value pairs of variables.
     *
     * @return {string}
     *   The HTML for the modal close button.
     */
    bootstrapPopoverXBody: function (variables) {
      var output = '';

      var defaults = {
        attributes: {
          class: ['popover-content', 'modal-body']
        },
        content: '',
        description: {
          attributes: {
            class: ['help-block']
          },
          content: null,
          position: 'before'
        },
        id: 'drupal-modal'
      };
      variables = $.extend(true, {}, defaults, variables);

      var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
      attributes.set('id', attributes.get('id', variables.id + '--content'));

      output += '<div' + attributes + '>';

      if (typeof variables.description === 'string') {
        variables.description = $.extend({}, defaults.description, { content: variables.description });
      }

      var description = variables.description;
      description.attributes = Attributes.create(defaults.description.attributes).merge(description.attributes);

      if (description.content && description.position === 'invisible') {
        description.attributes.addClass('sr-only');
      }

      if (description.content && description.position === 'before') {
        output += '<p' + description.attributes + '>' + description.content + '</p>';
      }

      output += variables.content;

      if (description.content && (description.position === 'after' || description.position === 'invisible')) {
        output += '<p' + description.attributes + '>' + description.content + '</p>';
      }

      output += '</div>';

      return output;
    },

    /**
     * Theme function for a Bootstrap PopoverX close button.
     *
     * @param {Object} [variables]
     *   An object containing key/value pairs of variables.
     *
     * @return {string}
     *   The HTML for the modal close button.
     */
    bootstrapPopoverXClose: function (variables) {
      var defaults = {
        attributes: {
          'aria-label': Drupal.t('Close'),
          class: ['close'],
          'data-dismiss': 'popover-x',
          type: 'button'
        }
      };
      variables = $.extend(true, {}, defaults, variables);
      var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
      return '<button' + attributes + '><span aria-hidden="true">&times;</span></button>';
    },

    /**
     * Theme function for a Bootstrap PopoverX footer.
     *
     * @param {Object} [variables]
     *   An object containing key/value pairs of variables.
     * @param {boolean} [force]
     *   Flag to force rendering the footer, regardless if there's content.
     *
     * @return {string}
     *   The HTML for the modal footer.
     */
    bootstrapPopoverXFooter: function (variables, force) {
      var output = '';
      var defaults = {
        attributes: {
          class: ['popover-footer', 'modal-footer']
        },
        footer: '',
        id: 'drupal-modal'
      };

      variables = $.extend(true, {}, defaults, variables);

      if (force || variables.footer) {
        var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
        attributes.set('id', attributes.get('id', variables.id + '--footer'));
        output += '<div' + attributes + '>';
        output += variables.footer;
        output += '</div>';
      }

      return output;
    },

    /**
     * Theme function for a Bootstrap PopoverX header.
     *
     * @param {Object} [variables]
     *   An object containing key/value pairs of variables.
     *
     * @return {string}
     *   The HTML for the modal header.
     */
    bootstrapPopoverXHeader: function (variables) {
      return Drupal.theme('bootstrapPopoverXTitle', variables);
    },

    /**
     * Theme function for a Bootstrap PopoverX title.
     *
     * @param {Object} [variables]
     *   An object containing key/value pairs of variables.
     *
     * @return {string}
     *   The HTML for the modal header.
     */
    bootstrapPopoverXTitle: function (variables) {
      var output = '';

      var defaults = {
        attributes: {
          class: ['popover-title', 'modal-header']
        },
        closeButton: true,
        id: 'drupal-modal',
        title: {
          content: '',
          html: false,
          tag: 'div'
        }
      };
      variables = $.extend(true, {}, defaults, variables);

      var title = variables.title;
      var attributes = Attributes.create(defaults.attributes).merge(variables.attributes);
      attributes.set('id', attributes.get('id', variables.id + '--title'));

      if (typeof title === 'string') {
        title = $.extend({}, defaults.title, { content: title });
      }

      output += '<div' + attributes + '>';

      if (variables.closeButton) {
        output += Drupal.theme('bootstrapPopoverXClose', _.omit(variables, 'attributes'));
      }

      output += '<' + Drupal.checkPlain(title.tag) + Attributes.create(defaults.title.attributes).merge(title.attributes) + '>' + (title.html ? title.content : Drupal.checkPlain(title.content)) + '</' + Drupal.checkPlain(title.tag) + '>';

      output += '</div>';

      return output;
    }
  });

})(jQuery, Drupal, Drupal.bootstrap);
